import React, { Component } from 'react';
import axios from 'axios';

export default class Register extends Component {

    constructor(props) {
        super(props);

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeRepeatPassword = this.onChangeRepeatPassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            username: '',
            password: '',
            repeat_password: ''
        }
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangeRepeatPassword(e) {
        this.setState({
            repeat_password: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        
        console.log(`Form submitted:`);
        console.log(`Username: ${this.state.username}`);
        console.log(`Password: ${this.state.password}`);
        console.log(`Repeated Password: ${this.state.repeat_password}`);
        
        const newUser = {
            username: this.state.username,
            password: this.state.password,
            repeat_password: this.state.repeat_password,
        };

     axios.post('http://localhost:4000/todos/register', newUser)
           .then(res => console.log(res.data));

        
        this.setState({
            username: '',
            password: '',
            repeat_password: ''
        })
    }

    render() {
        return (
            <div style={{marginTop: 10}}>
                <h3>Create an account</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Username: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.username}
                                onChange={this.onChangeUsername}
                                />
                    </div>
                    <div className="form-group">
                        <label>Password: </label>
                        <input 
                                type="Password" 
                                className="form-control"
                                value={this.state.password}
                                onChange={this.onChangePassword}
                                />
                    </div>
                                        <div className="form-group">
                        <label>Repeat Password: </label>
                        <input 
                                type="Password" 
                                className="form-control"
                                value={this.state.repeat_password}
                                onChange={this.onChangeRepeatPassword}
                                />
                    </div>


                    <div className="form-group">
                        <input type="submit" value="Register" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}